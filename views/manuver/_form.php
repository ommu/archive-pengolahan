<?php
/**
 * Archive Pengolahan Finals (archive-pengolahan-final)
 * @var $this app\components\View
 * @var $this ommu\archivePengolahan\controllers\FinalController
 * @var $model ommu\archivePengolahan\models\ArchivePengolahanFinal
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2022 OMMU (www.ommu.id)
 * @created date 13 November 2022, 12:03 WIB
 * @link https://bitbucket.org/ommu/archive-pengolahan
 *
 */

use yii\helpers\Html;
use app\components\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<div class="archive-pengolahan-final-form">

<?php $form = ActiveForm::begin([
	'options' => ['class' => 'form-horizontal form-label-left'],
	'enableClientValidation' => true,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
	'fieldConfig' => [
		'errorOptions' => [
			'encode' => false,
		],
	],
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php echo $form->field($model, 'fond_number')
	->textInput(['maxlength' => true])
	->label($model->getAttributeLabel('fond_number')); ?>

<?php echo $form->field($model, 'fond_name')
    ->textarea(['rows' => 3, 'cols' => 50])
    ->label($model->getAttributeLabel('fond_name')); ?>

<?php echo $form->field($model, 'archive_start_from')
	->textInput(['type' => 'number', 'min' => '1'])
	->label($model->getAttributeLabel('archive_start_from')); ?>

<?php $submitButtonOption = ['button' => Html::submitButton(Yii::t('app', 'Final Manuver'), ['class' => 'btn btn-warning'])];
echo $form->field($model, 'submitButton')
	->submitButton($submitButtonOption); ?>

<?php ActiveForm::end(); ?>

</div>